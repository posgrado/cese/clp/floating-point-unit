library ieee;
use ieee.std_logic_1164.all;

entity vector_tb is
end entity;

architecture vector_tb_arq of vector_tb is
    component vector is
        port (
            sig_i   : in std_logic;
            exp_i   : in std_logic_vector(7 downto 0);
            man_i   : in std_logic_vector(22 downto 0);
            vector_o    : out std_logic_vector(31 downto 0)  
        );
    end component;

    signal sig_tb : std_logic;
    signal exp_tb : std_logic_vector(7 downto 0);
    signal man_tb : std_logic_vector(22 downto 0);
    signal vector_o_tb : std_logic_vector(31 downto 0);  
begin
    sig_tb <= '0';
    exp_tb <= "10000000";
    man_tb <= "01000000000000000000000";
    DUT: vector
    port map (
        sig_i => sig_tb,
        exp_i => exp_tb,
        man_i => man_tb,
        vector_o => vector_o_tb
    );
end architecture;