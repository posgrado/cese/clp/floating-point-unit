library ieee;
use ieee.std_logic_1164.all;

entity n_subn_tb is
end entity;

architecture n_subn_tb_arq of n_subn_tb is
    component n_subn is
        port (
            numberA_i   : in std_logic_vector(36 downto 0);
            numberB_i   : in std_logic_vector(36 downto 0);
            comp_o      : out std_logic;
            sigA_o      : out std_logic;
            sigB_o      : out std_logic;
            exp_o      : out std_logic_vector(7 downto 0);
            manA_o      : out std_logic_vector(27 downto 0);
            manB_o      : out std_logic_vector(27 downto 0)
        );
    end component;

    signal numberA_tb   :   std_logic_vector(36 downto 0);
    signal numberB_tb   :   std_logic_vector(36 downto 0);
    signal comp_tb      : std_logic;
    signal sigA_tb      : std_logic;
    signal sigB_tb      : std_logic;
    signal exp_tb       : std_logic_vector(7 downto 0);
    signal manA_tb      : std_logic_vector(27 downto 0);
    signal manB_tb      : std_logic_vector(27 downto 0);

begin

    numberA_tb <=   "1000000000001000000000000000000000000",                 --
                    "1000000000001000000000000000000000000" after 10 ns,     --
                    "0000000000001000000000000000000000000" after 20 ns;     --

    numberB_tb <=   "1000000000100000000000000000000000000",                 --
                    "0000000000001000000000000000000000000" after 10 ns,     --
                    "0000000000100000000000000000000000000" after 20 ns;     --

    DUT: n_subn
    port map (
        numberA_i => numberA_tb,
        numberB_i => numberB_tb,
        comp_o => comp_tb,
        sigA_o => sigA_tb,
        sigB_o => sigB_tb,
        exp_o => exp_tb,
        manA_o => manA_tb,
        manB_o => manB_tb
    );
end architecture;