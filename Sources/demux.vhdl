library ieee;
use ieee.std_logic_1164.all;

entity demux is
    port (
        numberA_i       : in std_logic_vector(36 downto 0);        
        numberB_i       : in std_logic_vector(36 downto 0); 
        enable_type_i   : in std_logic_vector(1 downto 0);       
        numberA0_o       : out std_logic_vector(36 downto 0);        
        numberB0_o       : out std_logic_vector(36 downto 0); 
        numberA1_o       : out std_logic_vector(36 downto 0);        
        numberB1_o       : out std_logic_vector(36 downto 0); 
        numberA2_o       : out std_logic_vector(36 downto 0);        
        numberB2_o       : out std_logic_vector(36 downto 0)
    );
end entity;

architecture behavioral of demux is
begin
    process (numberA_i, numberB_i, enable_type_i)
    begin
        case enable_type_i is
            when "00" =>
                numberA0_o <= numberA_i;
                numberB0_o <= numberB_i;
                numberA1_o <= "-------------------------------------";
                numberB1_o <= "-------------------------------------";
                numberA2_o <= "-------------------------------------";
                numberB2_o <= "-------------------------------------";
            when "01" =>
                numberA0_o <= "-------------------------------------";
                numberB0_o <= "-------------------------------------";
                numberB1_o <= numberB_i;
                numberA1_o <= numberA_i;
                numberA2_o <= "-------------------------------------";
                numberB2_o <= "-------------------------------------";
            when "10" =>
                numberA0_o <= "-------------------------------------";
                numberB0_o <= "-------------------------------------";
                numberA1_o <= "-------------------------------------";
                numberB1_o <= "-------------------------------------";
                numberA2_o <= numberA_i;
                numberB2_o <= numberB_i;
            when others =>
                numberA0_o <= "-------------------------------------";
                numberB0_o <= "-------------------------------------";
                numberA1_o <= "-------------------------------------";
                numberB1_o <= "-------------------------------------";
                numberA2_o <= "-------------------------------------";
                numberB2_o <= "-------------------------------------";
        end case;
    end process;
end architecture;