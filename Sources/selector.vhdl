library ieee;
use ieee.std_logic_1164.all;

entity selector is
    port (
        numberA_i   : in std_logic_vector(31 downto 0);
        numberB_i   : in std_logic_vector(31 downto 0);
        enable_i    : in std_logic;
        enable_type_o   : out std_logic_vector(1 downto 0);
        numberA_o   : out std_logic_vector(36 downto 0);
        numberB_o   : out std_logic_vector(36 downto 0)        
    );
end entity;

architecture behavioral of selector is
    
    signal expA : std_logic_vector(7 downto 0);
    signal expB : std_logic_vector(7 downto 0);
    signal manA : std_logic_vector(22 downto 0);
    signal manB : std_logic_vector(22 downto 0);
    signal sigA : std_logic;
    signal sigB : std_logic;
    
begin
    sigA <= numberA_i(31);
    sigB <= numberB_i(31);
    expA <= numberA_i(30 downto 23);
    expB <= numberB_i(30 downto 23);
    manA <= numberA_i(22 downto 0);
    manB <= numberB_i(22 downto 0);

    process (sigA, sigB, manA, manB, expA, expB)
    begin
        if enable_i = '1' then
            numberA_o(36) <= sigA;
            numberA_o(35 downto 28) <= expA;
            numberB_o(36) <= sigB;
            numberB_o(35 downto 28) <= expB;
            if (expA > X"00") then
                numberA_o(27) <= '1';
                numberA_o(26 downto 4) <= manA;
                numberA_o(3 downto 0) <= X"0";
            elsif (expA = X"00") then
                numberA_o(27) <= '0';
                numberA_o(26 downto 4) <= manA;
                numberA_o(3 downto 0) <= X"0";
            else
                numberA_o <= "-------------------------------------";
            end if;

            if (expB > X"00") then
                numberB_o(27) <= '1';
                numberB_o(26 downto 4) <= manB;
                numberB_o(3 downto 0) <= X"0";
            elsif (expB = X"00") then
                numberB_o(27) <= '0';
                numberB_o(26 downto 4) <= manB;
                numberB_o(3 downto 0) <= X"0";
            else
                numberB_o <= "-------------------------------------";
            end if;
        else
            numberA_o <= "-------------------------------------";
            numberB_o <= "-------------------------------------";
        end if;
    end process;
    enable_type_o <=    "00" when expA = X"00" and expB = X"00" and enable_i = '1' else     -- Subnormals      
                        "01" when expA > X"00" and expB > X"00" and enable_i = '1' else     -- Normals
                        "10" when (expA = X"00" or expB = X"00") and enable_i = '1' else    -- Combination
                        "--";
end architecture;