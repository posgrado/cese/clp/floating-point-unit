library ieee;
use ieee.std_logic_1164.all;

entity mux_adder is
    port (
        norm_sigA_i     : in std_logic;        
        norm_sigB_i     : in std_logic;        
        subn_sigA_i     : in std_logic;        
        subn_sigB_i     : in std_logic;        
        comp_subn_i     : in std_logic;        
        comp_norm_i     : in std_logic;        
        norm_exp_i      : in std_logic_vector(7 downto 0);
        subn_exp_i      : in std_logic_vector(7 downto 0);
        norm_manA_i     : in std_logic_vector(27 downto 0);
        norm_manB_i     : in std_logic_vector(27 downto 0);
        subn_manA_i     : in std_logic_vector(27 downto 0);
        subn_manB_i     : in std_logic_vector(27 downto 0);
        enable_type_i   : in std_logic_vector(1 downto 0);
        sigA_o  : out std_logic;        
        sigB_o  : out std_logic;        
        comp_o  : out std_logic;        
        exp_o   : out std_logic_vector(7 downto 0);
        manA_o  : out std_logic_vector(27 downto 0);
        manB_o  : out std_logic_vector(27 downto 0)
    );
end entity;

architecture behavioral of mux_adder is
    
begin

    manA_o <=   norm_manA_i when enable_type_i = "01" or enable_type_i = "10" else
                subn_manA_i when enable_type_i = "00" else
                "----------------------------";

    manB_o <=   norm_manB_i when enable_type_i = "01" or enable_type_i = "10" else
                subn_manB_i when enable_type_i = "00" else
                "----------------------------";

    comp_o <=   comp_norm_i when enable_type_i = "01" or enable_type_i = "10" else
                comp_subn_i when enable_type_i = "00" else
                '-';            

    sigA_o <=   norm_sigA_i when enable_type_i = "01" or enable_type_i = "10" else
                subn_sigA_i when enable_type_i = "00" else
                '-';           

    sigB_o <=   norm_sigB_i when enable_type_i = "01" or enable_type_i = "10" else
                subn_sigB_i when enable_type_i = "00" else
                '-';           
                 
    exp_o <=    norm_exp_i when enable_type_i = "01" or enable_type_i = "10" else
                subn_exp_i when enable_type_i = "00" else
                "--------";       

end architecture;