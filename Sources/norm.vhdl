library ieee;
use ieee.std_logic_1164.all;

entity norm is
    port (
        numberA_i   : in std_logic_vector(36 downto 0);        
        numberB_i   : in std_logic_vector(36 downto 0);        
        normA_o   : out std_logic_vector(36 downto 0);        
        normB_o   : out std_logic_vector(36 downto 0)
    );
end entity;

architecture behavioral of norm is
    
    component zero_counter is
        port (
            number_i    : in std_logic_vector(27 downto 0);
            z_count_o   : out std_logic_vector(4 downto 0)  
        );
    end component;

    component shift_left is
        port (
            number_i : in std_logic_vector(27 downto 0);
            shift_i : in std_logic_vector(4 downto 0);
            shifted_o : out std_logic_vector(27 downto 0)
        );
    end component;

    component comp is
        port (
            numberA_i   : in std_logic_vector(36 downto 0);
            numberB_i   : in std_logic_vector(36 downto 0);
            normal_o   : out std_logic_vector(36 downto 0);
            subnormal_o   : out std_logic_vector(36 downto 0)
        );
    end component;

    signal zero_counter_aux : std_logic_vector(4 downto 0);
    signal expB_aux : std_logic_vector(7 downto 0);
    signal manB_aux : std_logic_vector(27 downto 0);
    signal numberB_aux : std_logic_vector(36 downto 0);
    
begin
    zero_counter_inst: zero_counter
        port map (
            number_i => numberB_aux(27 downto 0),
            z_count_o => zero_counter_aux
        );
    
    shift_left_inst: shift_left
        port map (
            number_i => numberB_aux(27 downto 0),
            shift_i => zero_counter_aux,
            shifted_o => manB_aux
        );

    comp_inst: comp
        port map (
            numberA_i => numberA_i,
            numberB_i => numberB_i,
            normal_o => normA_o,
            subnormal_o => numberB_aux           
        );

    process (zero_counter_aux, numberB_aux, expB_aux, manB_aux)
    begin

        if zero_counter_aux /= "-----" then
            expB_aux <= "000" & zero_counter_aux;
            normB_o(27 downto 0) <= manB_aux(27 downto 1) & '1';
        else
            expB_aux <= "--------";
            normB_o(27 downto 0) <= manB_aux;
        end if;
        normB_o(35 downto 28) <= expB_aux;
        normB_o(36) <= numberB_aux(36);
    end process;
end architecture;