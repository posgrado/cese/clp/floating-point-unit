library ieee;
use ieee.std_logic_1164.all;

entity zero_counter is
    port (
        number_i    : in std_logic_vector(27 downto 0);
        z_count_o   : out std_logic_vector(4 downto 0)        
    );
end entity;

architecture behavioral of zero_counter is
    signal aux : std_logic_vector(7 downto 0);
    signal zero_vector : std_logic_vector(27 downto 0) := "0000000000000000000000000000";
    
begin

    aux <= "--------" when number_i(27 downto 27) = "-" else
           X"1C" when number_i(27 downto 0) = zero_vector(27 downto 0) else
           X"1B" when number_i(27 downto 1) = zero_vector(27 downto 1) else
           X"1A" when number_i(27 downto 2) = zero_vector(27 downto 2) else
           X"19" when number_i(27 downto 3) = zero_vector(27 downto 3) else
           X"18" when number_i(27 downto 4) = zero_vector(27 downto 4) else
           X"17" when number_i(27 downto 5) = zero_vector(27 downto 5) else
           X"16" when number_i(27 downto 6) = zero_vector(27 downto 6) else
           X"15" when number_i(27 downto 7) = zero_vector(27 downto 7) else
           X"14" when number_i(27 downto 8) = zero_vector(27 downto 8) else
           X"13" when number_i(27 downto 9) = zero_vector(27 downto 9) else
           X"12" when number_i(27 downto 10) = zero_vector(27 downto 10) else
           X"11" when number_i(27 downto 11) = zero_vector(27 downto 11) else
           X"10" when number_i(27 downto 12) = zero_vector(27 downto 12) else
           X"0F" when number_i(27 downto 13) = zero_vector(27 downto 13) else
           X"0E" when number_i(27 downto 14) = zero_vector(27 downto 14) else
           X"0D" when number_i(27 downto 15) = zero_vector(27 downto 15) else
           X"0C" when number_i(27 downto 16) = zero_vector(27 downto 16) else
           X"0B" when number_i(27 downto 17) = zero_vector(27 downto 17) else
           X"0A" when number_i(27 downto 18) = zero_vector(27 downto 18) else
           X"09" when number_i(27 downto 19) = zero_vector(27 downto 19) else
           X"08" when number_i(27 downto 20) = zero_vector(27 downto 20) else
           X"07" when number_i(27 downto 21) = zero_vector(27 downto 21) else
           X"06" when number_i(27 downto 22) = zero_vector(27 downto 22) else
           X"05" when number_i(27 downto 23) = zero_vector(27 downto 23) else
           X"04" when number_i(27 downto 24) = zero_vector(27 downto 24) else
           X"03" when number_i(27 downto 25) = zero_vector(27 downto 25) else
           X"02" when number_i(27 downto 26) = zero_vector(27 downto 26) else
           X"01" when number_i(27 downto 27) = zero_vector(27 downto 27) else
           X"00";

    z_count_o <= aux(4 downto 0);
end architecture;