library ieee;
use ieee.std_logic_1164.all;
USE IEEE.std_logic_unsigned.ALL;

entity block_adder is
    port (
        sigA_i      : in std_logic;
        sigB_i      : in std_logic;
        numberA_i   : in std_logic_vector(27 downto 0);
        numberB_i   : in std_logic_vector(27 downto 0);
        addSub_i    : in std_logic;
        comp_i      : in std_logic;
        result_o    : out std_logic_vector(27 downto 0);
        sig_o       : out std_logic;
        carry_o       : out std_logic
    );
end entity;

architecture behavioral of block_adder is
    
    component adder is
        port (
            numberA_i   : in std_logic_vector(27 downto 0);        
            numberB_i   : in std_logic_vector(27 downto 0);        
            addSub_i    : in std_logic;
            result_o    : out std_logic_vector(27 downto 0);
            carry_o     : out std_logic   
        );
    end component;

    component signout is
        port (
            sigA_i      : in std_logic;
            sigB_i      : in std_logic;
            numberA_i   : in std_logic_vector(27 downto 0);
            numberB_i   : in std_logic_vector(27 downto 0);
            addSub_i    : in std_logic;
            comp_i      : in std_logic;
            numberA_o   : out std_logic_vector(27 downto 0);
            numberB_o   : out std_logic_vector(27 downto 0);
            addSub_o    : out std_logic;
            sig_o      : out std_logic
        );
    end component;

    signal numberA_aux : std_logic_vector(27 downto 0);
    signal numberB_aux : std_logic_vector(27 downto 0);
    signal result_aux : std_logic_vector(27 downto 0);
    signal addSub_aux  : std_logic;
    signal sig_aux  : std_logic;
    signal carry_aux  : std_logic;
    
begin

    signout_inst: signout
        port map (
            sigA_i => sigA_i,
            sigB_i => sigB_i,
            numberA_i => numberA_i,
            numberB_i => numberB_i,
            addSub_i => addSub_i,
            comp_i => comp_i,
            numberA_o => numberA_aux,
            numberB_o => numberB_aux,
            addSub_o => addSub_aux,
            sig_o => sig_aux
        );

    adder_inst: adder
        port map (
            numberA_i => numberA_aux,
            numberB_i => numberB_aux,
            addSub_i => addSub_aux,
            result_o => result_aux,
            carry_o => carry_aux
        );

    -- If a complement to 1 is used and output sign is 1, then complement to 2 is needed
    result_o <= (result_aux xor X"FFFFFFF") + '1' when (( addSub_aux and sig_aux ) = '1') else
                result_aux;

    carry_o <= '0' when ((sigB_i xor addSub_i) /= sigA_i) else
                carry_aux;
                
    sig_o <= sig_aux;

end architecture;