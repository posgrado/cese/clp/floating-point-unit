library ieee;
use ieee.std_logic_1164.all;

entity mux_1b is
    port (
        bitA_i  : in std_logic;
        bitB_i  : in std_logic;
        sel_i  : in std_logic;
        bit_o  : out std_logic
    );
end entity;

architecture behavioral of mux_1b is
    
begin
    bit_o <= bitA_i when sel_i = '1' else
            bitB_i when sel_i = '0' else
            '-';
end architecture;