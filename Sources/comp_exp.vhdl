library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity comp_exp is
    port (
        numberA_i   : in std_logic_vector(36 downto 0);        
        numberB_i   : in std_logic_vector(36 downto 0);        
        sigA_o      : out std_logic;
        sigB_o      : out std_logic;
        max_exp_o   : out std_logic_vector(7 downto 0);
        max_man_o   : out std_logic_vector(27 downto 0);
        shift_man_o : out std_logic_vector(27 downto 0);
        exp_diff_o  : out std_logic_vector(4 downto 0);
        comp_o      : out std_logic
    );
end entity;

architecture behavioral of comp_exp is

    signal expA : std_logic_vector(7 downto 0);
    signal expB : std_logic_vector(7 downto 0);
    signal manA : std_logic_vector(27 downto 0);
    signal manB : std_logic_vector(27 downto 0);
    
    signal diff : std_logic_vector(7 downto 0);
    signal comp : std_logic;

begin
    sigA_o <= numberA_i(36);
    sigB_o <= numberB_i(36);

    expA <= numberA_i(35 downto 28);
    expB <= numberB_i(35 downto 28);
    manA <= numberA_i(27 downto 0);
    manB <= numberB_i(27 downto 0);

    comp <= '1' when (expA > expB) or (manB(0) = '1') else
            '0' when (expA < expB) else
            '1' when (manA >= manB) else
            '0' when (manA < manB) else
            '-';
    comp_o <= comp;

    max_exp_o <=    expA when comp = '1' else
                    expB when comp = '0' else
                    "--------";

    diff <= expA - expB when comp = '1' and manB(0) = '0' else
            expB - expA when comp = '0'else
            expA + expB when comp = '1' and manB(0) = '1' else
            "--------";

    process (diff)
    begin
        if diff <= X"1B" then
            exp_diff_o <= diff(4 downto 0);
        elsif diff > X"1B" then
            exp_diff_o <= "11100";
        else
            exp_diff_o <= "-----";
        end if;
    end process;

    shift_man_o <=  manB when comp = '1' else
                    manA when comp = '0' else
                    "----------------------------";
    max_man_o <=    manA when comp = '1' else
                    manB when comp = '0' else
                    "----------------------------";
end architecture;