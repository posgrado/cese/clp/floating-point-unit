library ieee;
use ieee.std_logic_1164.all;

entity adder is
    port (
        numberA_i   : in std_logic_vector(27 downto 0);        
        numberB_i   : in std_logic_vector(27 downto 0);        
        addSub_i    : in std_logic;
        result_o    : out std_logic_vector(27 downto 0);
        carry_o     : out std_logic 
    );
end entity;

architecture behavioral of adder is
    component adder_1b is
        port (
            bitA_i  : in std_logic;
            bitB_i  : in std_logic;
            carry_i  : in std_logic;
            sum_o  : out std_logic;
            carry_o  : out std_logic
        );
    end component;

    signal numberB_aux : std_logic_vector(27 downto 0);
    signal aux : std_logic_vector(27 downto 0);
    signal result_aux : std_logic_vector(27 downto 0);
    

begin
    gen_loop_adder : for i in 0 to 27 generate
        numberB_aux(i) <= numberB_i(i) xor addSub_i;
        
        gen_addder_0 : if (i = 0) generate
            adder_0_inst: adder_1b
                port map (
                    bitA_i => numberA_i(i),
                    bitB_i => numberB_aux(i),
                    carry_i => addSub_i,
                    sum_o => result_aux(i),
                    carry_o => aux(i)                    
                );
        end generate;
        
        gen_adder_i : if ((i > 0) and (i<28)) generate
            adder_i_inst: adder_1b
                port map (
                    bitA_i => numberA_i(i),
                    bitB_i => numberB_aux(i),
                    carry_i => aux(i-1),
                    sum_o => result_aux(i),
                    carry_o => aux(i)     
                );
        end generate;
    end generate;

    result_o <= result_aux;
    carry_o <= aux(27);

end architecture;