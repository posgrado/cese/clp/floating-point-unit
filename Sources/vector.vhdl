library ieee;
use ieee.std_logic_1164.all;

entity vector is
    port (
        sig_i   : in std_logic;
        exp_i   : in std_logic_vector(7 downto 0);
        man_i   : in std_logic_vector(22 downto 0);
        vector_o    : out std_logic_vector(31 downto 0)  
    );
end entity;

architecture behavioral of vector is
    
begin
        vector_o(31) <= sig_i;
        vector_o(30 downto 23) <= exp_i;
        vector_o(22 downto 0) <= man_i;
end architecture;