library ieee;
use ieee.std_logic_1164.all;

entity n_normal is
    port (
        numberA_i   : in std_logic_vector(36 downto 0);        
        numberB_i   : in std_logic_vector(36 downto 0);        
        comp_o      : out std_logic;        
        sigA_o      : out std_logic;        
        sigB_o      : out std_logic;        
        exp_o       : out std_logic_vector(7 downto 0);        
        manA_o      : out std_logic_vector(27 downto 0);        
        manB_o      : out std_logic_vector(27 downto 0)        
    );
end entity;

architecture behavioral of n_normal is
    component comp_exp is
        port (
            numberA_i   : in std_logic_vector(36 downto 0);        
            numberB_i   : in std_logic_vector(36 downto 0);        
            sigA_o      : out std_logic;
            sigB_o      : out std_logic;
            max_exp_o   : out std_logic_vector(7 downto 0);
            max_man_o   : out std_logic_vector(27 downto 0);
            shift_man_o : out std_logic_vector(27 downto 0);
            exp_diff_o  : out std_logic_vector(4 downto 0);
            comp_o      : out std_logic
        );
    end component;

    component shift is
        port (
            number_i : in std_logic_vector(27 downto 0);
            shift_i : in std_logic_vector(4 downto 0);
            shifted_o : out std_logic_vector(27 downto 0)
        );
    end component;

    signal man_shift_aux : std_logic_vector(27 downto 0);
    signal diff_exp_aux : std_logic_vector(4 downto 0);
    
begin

    comp_exp_inst: comp_exp
        port map (
            numberA_i => numberA_i,
            numberB_i => numberB_i,
            sigA_o => sigA_o,
            sigB_o => sigB_o,
            max_exp_o => exp_o,
            max_man_o => manA_o,
            shift_man_o => man_shift_aux,
            exp_diff_o => diff_exp_aux,
            comp_o => comp_o    
        );

    shift_inst: shift
        port map (
            number_i => man_shift_aux,
            shift_i => diff_exp_aux,
            shifted_o => manB_o            
        );
end architecture;