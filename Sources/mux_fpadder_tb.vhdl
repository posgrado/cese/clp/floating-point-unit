library ieee;
use ieee.std_logic_1164.all;

entity mux_fpadder_tb is
end entity;

architecture mux_fpadder_tb_arq of mux_fpadder_tb is
    component mux_fpadder is
        port (
            n_case_number_i : in std_logic_vector(31 downto 0);
            adder_number_i  : in std_logic_vector(31 downto 0);
            enable_i        : in std_logic;
            result_o        : out std_logic_vector(31 downto 0)
        );
    end component;


    signal n_case_number_tb: std_logic_vector(31 downto 0);
    signal adder_number_tb: std_logic_vector(31 downto 0);
    signal enable_tb: std_logic;
    signal result_tb: std_logic_vector(31 downto 0);

begin
    n_case_number_tb <= "11111111100000000000000000000000";
    adder_number_tb <= "00000000000000000000000000000000";
    enable_tb <= '0', '1' after 100 ns;

    DUT: mux_fpadder
    port map (
        n_case_number_i => n_case_number_tb,
        adder_number_i => adder_number_tb,  
        enable_i => enable_tb,
        result_o => result_tb       
    );
end architecture;