library ieee;
use ieee.std_logic_1164.all;

entity n_subn is
    port (
        numberA_i   : in std_logic_vector(36 downto 0);
        numberB_i   : in std_logic_vector(36 downto 0);
        comp_o      : out std_logic;
        sigA_o      : out std_logic;
        sigB_o      : out std_logic;
        exp_o      : out std_logic_vector(7 downto 0);
        manA_o      : out std_logic_vector(27 downto 0);
        manB_o      : out std_logic_vector(27 downto 0)
    );
end entity;

architecture behavioral of n_subn is
    signal manA : std_logic_vector(27 downto 0);
    signal manB : std_logic_vector(27 downto 0);
    signal comp : std_logic;
    
begin
    sigA_o <= numberA_i(36);
    sigB_o <= numberB_i(36);

    manA <= numberA_i(27 downto 0);
    manB <= numberB_i(27 downto 0);

    comp <= '1' when manA >= manB else
            '0' when manB > manA else
            '-';
    comp_o <= comp;

    exp_o <= numberA_i(35 downto 28);

    manB_o <= manB when comp = '1' else
            manA when comp = '0' else
            "----------------------------";
    
    manA_o <= manA when comp = '1' else
            manB when comp = '0' else
            "----------------------------";
end architecture;
