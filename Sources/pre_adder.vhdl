library ieee;
use ieee.std_logic_1164.all;

entity pre_adder is
    port (
        numberA_i   : in std_logic_vector(31 downto 0);
        numberB_i   : in std_logic_vector(31 downto 0);
        enable_i    : in std_logic;
        sigA_o      : out std_logic;
        sigB_o      : out std_logic;
        comp_o      : out std_logic;
        exp_o       : out std_logic_vector(7 downto 0);
        manA_o       : out std_logic_vector(27 downto 0);
        manB_o       : out std_logic_vector(27 downto 0)
    );
end entity;

architecture behavioral of pre_adder is
    component selector is
        port (
            numberA_i   : in std_logic_vector(31 downto 0);
            numberB_i   : in std_logic_vector(31 downto 0);
            enable_i    : in std_logic;
            enable_type_o   : out std_logic_vector(1 downto 0);
            numberA_o   : out std_logic_vector(36 downto 0);
            numberB_o   : out std_logic_vector(36 downto 0)       
        );
    end component;

    component demux is
        port (
            numberA_i       : in std_logic_vector(36 downto 0);        
            numberB_i       : in std_logic_vector(36 downto 0); 
            enable_type_i   : in std_logic_vector(1 downto 0);       
            numberA0_o       : out std_logic_vector(36 downto 0);        
            numberB0_o       : out std_logic_vector(36 downto 0); 
            numberA1_o       : out std_logic_vector(36 downto 0);        
            numberB1_o       : out std_logic_vector(36 downto 0); 
            numberA2_o       : out std_logic_vector(36 downto 0);        
            numberB2_o       : out std_logic_vector(36 downto 0)
        );
    end component;

    component n_subn is
        port (
            numberA_i   : in std_logic_vector(36 downto 0);
            numberB_i   : in std_logic_vector(36 downto 0);
            comp_o      : out std_logic;
            sigA_o      : out std_logic;
            sigB_o      : out std_logic;
            exp_o      : out std_logic_vector(7 downto 0);
            manA_o      : out std_logic_vector(27 downto 0);
            manB_o      : out std_logic_vector(27 downto 0)
        );
    end component;

    component mux_ns is
        port (
            normalA_i       : in std_logic_vector(36 downto 0);        
            normalB_i       : in std_logic_vector(36 downto 0);        
            mixedA_i        : in std_logic_vector(36 downto 0);        
            mixedB_i        : in std_logic_vector(36 downto 0);  
            enable_type_i   : in std_logic_vector(1 downto 0);
            numberA_o       : out std_logic_vector(36 downto 0);  
            numberB_o       : out std_logic_vector(36 downto 0)
        );
    end component;

    component norm is
        port (
            numberA_i   : in std_logic_vector(36 downto 0);        
            numberB_i   : in std_logic_vector(36 downto 0);        
            normA_o   : out std_logic_vector(36 downto 0);        
            normB_o   : out std_logic_vector(36 downto 0)
        );
    end component;

    component n_normal is
        port (
            numberA_i   : in std_logic_vector(36 downto 0);        
            numberB_i   : in std_logic_vector(36 downto 0);        
            comp_o      : out std_logic;        
            sigA_o      : out std_logic;        
            sigB_o      : out std_logic;        
            exp_o       : out std_logic_vector(7 downto 0);        
            manA_o      : out std_logic_vector(27 downto 0);        
            manB_o      : out std_logic_vector(27 downto 0)        
        );
    end component;

    component mux_adder is
        port (
            norm_sigA_i     : in std_logic;        
            norm_sigB_i     : in std_logic;        
            subn_sigA_i     : in std_logic;        
            subn_sigB_i     : in std_logic;        
            comp_subn_i     : in std_logic;        
            comp_norm_i     : in std_logic;        
            norm_exp_i      : in std_logic_vector(7 downto 0);
            subn_exp_i      : in std_logic_vector(7 downto 0);
            norm_manA_i     : in std_logic_vector(27 downto 0);
            norm_manB_i     : in std_logic_vector(27 downto 0);
            subn_manA_i     : in std_logic_vector(27 downto 0);
            subn_manB_i     : in std_logic_vector(27 downto 0);
            enable_type_i   : in std_logic_vector(1 downto 0);
            sigA_o  : out std_logic;        
            sigB_o  : out std_logic;        
            comp_o  : out std_logic;        
            exp_o   : out std_logic_vector(7 downto 0);
            manA_o  : out std_logic_vector(27 downto 0);
            manB_o  : out std_logic_vector(27 downto 0)
        );
    end component;

    signal numberA_o_select   : std_logic_vector(36 downto 0);
    signal numberB_o_select   : std_logic_vector(36 downto 0);
    signal subnA   : std_logic_vector(36 downto 0);
    signal subnB   : std_logic_vector(36 downto 0);
    signal normA   : std_logic_vector(36 downto 0);
    signal normB   : std_logic_vector(36 downto 0);
    signal mixA   : std_logic_vector(36 downto 0);
    signal mixB   : std_logic_vector(36 downto 0);
    
    signal comp_subn   : std_logic;
    signal sigA_subn   : std_logic;
    signal sigB_subn   : std_logic;
    signal exp_subn   : std_logic_vector(7 downto 0);
    signal manA_subn   : std_logic_vector(27 downto 0);
    signal manB_subn   : std_logic_vector(27 downto 0);
    
    signal nomalizedA   : std_logic_vector(36 downto 0);
    signal nomalizedB   : std_logic_vector(36 downto 0);
    
    signal muxA   : std_logic_vector(36 downto 0);
    signal muxB   : std_logic_vector(36 downto 0);
    
    signal comp_norm   : std_logic;
    signal sigA_norm   : std_logic;
    signal sigB_norm   : std_logic;
    signal exp_norm   : std_logic_vector(7 downto 0);
    signal manA_norm   : std_logic_vector(27 downto 0);
    signal manB_norm   : std_logic_vector(27 downto 0);

    signal enable_type   : std_logic_vector(1 downto 0);
begin

    selector_inst: selector
        port map (
            numberA_i => numberA_i,
            numberB_i => numberB_i,
            enable_i => enable_i,
            enable_type_o => enable_type,
            numberA_o =>numberA_o_select,
            numberB_o =>numberB_o_select
        );
        
    demux_inst: demux
        port map (
            numberA_i => numberA_o_select,
            numberB_i => numberB_o_select,
            enable_type_i => enable_type,
            numberA0_o => subnA,
            numberB0_o => subnB,
            numberA1_o => normA,
            numberB1_o => normB,
            numberA2_o => mixA,
            numberB2_o => mixB
        );

    n_subn_inst: n_subn
        port map (
            numberA_i => subnA,
            numberB_i => subnB,
            comp_o => comp_subn,
            sigA_o => sigA_subn,
            sigB_o => sigB_subn,
            exp_o => exp_subn,
            manA_o => manA_subn, 
            manB_o => manB_subn 
        );

    mux_ns_inst: mux_ns
        port map (
            normalA_i => normA,
            normalB_i => normB,
            mixedA_i => nomalizedA,
            mixedB_i => nomalizedB,
            enable_type_i => enable_type,
            numberA_o => muxA,
            numberB_o => muxB
        );

    norm_inst: norm
        port map (
            numberA_i => mixA,
            numberB_i => mixB,
            normA_o => nomalizedA,
            normB_o => nomalizedB
        );

    n_normal_inst: n_normal
        port map (
            numberA_i => muxA,
            numberB_i => muxB,
            comp_o => comp_norm,
            sigA_o => sigA_norm,
            sigB_o => sigB_norm,
            exp_o => exp_norm,
            manA_o => manA_norm,
            manB_o => manB_norm
        );

    mux_adder_inst: mux_adder
        port map (
            norm_sigA_i => sigA_norm,
            norm_sigB_i => sigB_norm,
            subn_sigA_i => sigA_subn,
            subn_sigB_i => sigB_subn,
            comp_subn_i => comp_subn,
            comp_norm_i => comp_norm,
            norm_exp_i => exp_norm,
            subn_exp_i => exp_subn,
            norm_manA_i => manA_norm,
            norm_manB_i => manB_norm,
            subn_manA_i => manA_subn,
            subn_manB_i => manB_subn,
            enable_type_i => enable_type,
            sigA_o => sigA_o,
            sigB_o => sigB_o,
            comp_o => comp_o,
            exp_o => exp_o,
            manA_o => manA_o,
            manB_o => manB_o
        );

end architecture;