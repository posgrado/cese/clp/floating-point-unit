library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity round is
    port (
        man_i   : in std_logic_vector(27 downto 0);
        man_o   : out   std_logic_vector(22 downto 0)        
    );
end entity;

architecture behavioral of round is
    
    signal man_aux : std_logic_vector(22 downto 0);
    
begin
    process (man_i)
    begin
        if man_i(3 downto 0) = "----" then
            man_aux <= "-----------------------";
        elsif man_i(3 downto 0) >= "1000" then
            man_aux <= man_i(26 downto 4) + '1';
        else
            man_aux <= man_i(26 downto 4);
        end if;
    end process;
    man_o <= man_aux;
end architecture;