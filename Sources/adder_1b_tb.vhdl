library ieee;
use ieee.std_logic_1164.all;

entity adder_1b_tb is
end entity;

architecture adder_1b_tb_arq of adder_1b_tb is
    component adder_1b is
        port (
            bitA_i  : in std_logic;
            bitB_i  : in std_logic;
            carry_i  : in std_logic;
            sum_o  : out std_logic;
            carry_o  : out std_logic
        );
    end component;

    signal bitA_tb      : std_logic := '0';
    signal bitB_tb      : std_logic := '0';
    signal carry_i_tb   : std_logic := '0';
    signal sum_tb       : std_logic;
    signal carry_o_tb   : std_logic;

begin
    bitA_tb <= not bitA_tb after 10 ns;
    bitB_tb <= not bitB_tb after 20 ns;
    carry_i_tb <= not carry_i_tb after 40 ns;

    DUT: adder_1b
    port map (
        bitA_i => bitA_tb,
        bitB_i => bitB_tb,
        carry_i => carry_i_tb,
        sum_o => sum_tb,
        carry_o => carry_o_tb
    );
end architecture;