library ieee;
use ieee.std_logic_1164.all;

entity selector_tb is
end entity;

architecture selector_tb_arq of selector_tb is
    component selector is
        port (
            numberA_i   : in std_logic_vector(31 downto 0);
            numberB_i   : in std_logic_vector(31 downto 0);
            enable_i    : in std_logic;
            enable_type_o   : out std_logic_vector(1 downto 0);
            numberA_o   : out std_logic_vector(36 downto 0);
            numberB_o   : out std_logic_vector(36 downto 0)      
        );
    end component;

    signal numberA_i_tb     : std_logic_vector(31 downto 0);
    signal numberB_i_tb     : std_logic_vector(31 downto 0);
    signal enable_tb        : std_logic;
    signal enable_type_tb   : std_logic_vector(1 downto 0);
    signal numberA_o_tb     : std_logic_vector(36 downto 0);
    signal numberB_o_tb     : std_logic_vector(36 downto 0);  
begin

    numberA_i_tb <= "01000000000110011001100110011010",                 -- 2.4 (normal)
                    "01000000000000000000000000000000" after 10 ns,     -- 2 (normal)
                    "00000000001000000000000000000000" after 20 ns,     -- 2.93873587706e-39 (subnormal)
                    "01000000000000000000000000000000" after 30 ns;     -- 2 (normal)
                    
    numberB_i_tb <= "01000000000110011001100110011010",                 -- 2.4 (normal
                    "01000000000000000000000000000000" after 10 ns,     -- 2 (normal)
                    "00000000001000000000000000000000" after 20 ns,     -- 2.93873587706e-39 (subnormal)
                    "00000000001000000000000000000000" after 30 ns;     -- 2.93873587706e-39 (subnormal)

    enable_tb <= '1';

    DUT: selector
    port map (
        numberA_i => numberA_i_tb,
        numberB_i => numberB_i_tb,
        numberA_o => numberA_o_tb,
        numberB_o => numberB_o_tb,
        enable_i => enable_tb,
        enable_type_o => enable_type_tb
    );
end architecture;