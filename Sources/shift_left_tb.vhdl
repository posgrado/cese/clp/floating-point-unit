library ieee;
use ieee.std_logic_1164.all;

entity shift_left_tb is
end entity;

architecture shift_left_tb_arq of shift_left_tb is
    component shift_left is
        port (
            number_i : in std_logic_vector(27 downto 0);
            shift_i : in std_logic_vector(4 downto 0);
            shifted_o : out std_logic_vector(27 downto 0)
        );
    end component;

    signal number_tb    : std_logic_vector(27 downto 0);
    signal shift_tb     : std_logic_vector(4 downto 0);
    signal shifted_tb   : std_logic_vector(27 downto 0);

begin

    number_tb <= "0000000000000000000000000001";
    shift_tb <= "00000", 
                "00001" after 10 ns,
                "00010" after 20 ns,
                "00100" after 30 ns,
                "01000" after 40 ns,
                "10000" after 50 ns,
                "11011" after 60 ns;

    DUT: shift_left
    port map (
        number_i => number_tb,
        shift_i => shift_tb,
        shifted_o => shifted_tb
    );
end architecture;