library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity block_norm is
    port (
        man_i   : in std_logic_vector(27 downto 0);
        exp_i   : in std_logic_vector(7 downto 0);
        carry_i : in std_logic;
        man_o   : out std_logic_vector(22 downto 0);
        exp_o   : out std_logic_vector(7 downto 0)
    );
end entity;

architecture behavioral of block_norm is
    component zero_counter is
        port (
            number_i    : in std_logic_vector(27 downto 0);
            z_count_o   : out std_logic_vector(4 downto 0)  
        );
    end component;

    component shift_left is
        port (
            number_i : in std_logic_vector(27 downto 0);
            shift_i : in std_logic_vector(4 downto 0);
            shifted_o : out std_logic_vector(27 downto 0)
        );
    end component;

    component round is
        port (
            man_i   : in std_logic_vector(27 downto 0);
            man_o   : out   std_logic_vector(22 downto 0)                    
        );
    end component;
    signal z_count_aux : std_logic_vector(4 downto 0);
    signal shift : std_logic_vector(4 downto 0);
    signal number : std_logic_vector(27 downto 0);
    
begin
    zero_count_inst: zero_counter
        port map (
            number_i => man_i,
            z_count_o => z_count_aux            
        );
    
    shift_left_inst: shift_left
        port map (
            number_i   => man_i,
            shift_i => shift,
            shifted_o => number            
        );
    round_inst: round
        port map (
            man_i   => number,
            man_o => man_o            
        );

    process (man_i, exp_i, shift, z_count_aux, carry_i)
    begin
        if z_count_aux = "-----" then
            shift <= "-----";
            exp_o <= "--------";
        elsif exp_i > z_count_aux then
            shift <= z_count_aux;
            exp_o <= exp_i - shift + carry_i;
        elsif exp_i < z_count_aux then
            shift <= exp_i(4 downto 0);
            exp_o <= X"00";
        elsif exp_i = z_count_aux then
            shift <= z_count_aux;
            exp_o <= X"01";
        end if;
    end process;
end architecture;