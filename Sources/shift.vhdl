library ieee;
use ieee.std_logic_1164.all;

entity shift is
    port (
       number_i : in std_logic_vector(27 downto 0);
       shift_i : in std_logic_vector(4 downto 0);
       shifted_o : out std_logic_vector(27 downto 0)
    );
end entity;

architecture behavioral of shift is
    component mux_1b is
        port (
            bitA_i  : in std_logic;
            bitB_i  : in std_logic;
            sel_i  : in std_logic;
            bit_o  : out std_logic
        );
    end component;
    
    signal shifted0 : std_logic_vector(27 downto 0) := "0000000000000000000000000000";
    signal shifted1 : std_logic_vector(27 downto 0) := "0000000000000000000000000000";
    signal shifted2 : std_logic_vector(27 downto 0) := "0000000000000000000000000000";
    signal shifted3 : std_logic_vector(27 downto 0) := "0000000000000000000000000000";
    signal shifted4 : std_logic_vector(27 downto 0) := "0000000000000000000000000000";

begin
    gen_loop_shift : for i in 0 to 27 generate
        gen_shift0_0 : if (i = 0) generate
            shifter0_0_inst: mux_1b
                port map (
                    bitA_i => '0',
                    bitB_i => number_i(27),
                    sel_i => shift_i(0),
                    bit_o => shifted0(27 - i)                    
                );
        end generate;
        gen_shift0_i : if (i > 0) and (i < 28) generate
            shifter0_i_inst: mux_1b
                port map (
                    bitA_i => number_i(27 - (i - 1)),
                    bitB_i => number_i(27 - i),
                    sel_i => shift_i(0),
                    bit_o => shifted0(27 - i)                    
                );
        end generate;
        gen_shift1_0 : if (i >= 0) and (i < 2) generate
            shifter1_0_inst: mux_1b
                port map (
                    bitA_i => '0',
                    bitB_i => shifted0(27 - i),
                    sel_i => shift_i(1),
                    bit_o => shifted1(27 - i)                    
                );
        end generate;
        gen_shift1_i : if (i > 1) and (i < 28) generate
            shifter1_i_inst: mux_1b
                port map (
                    bitA_i => shifted0(27 - (i - 2)),
                    bitB_i => shifted0(27 - i),
                    sel_i => shift_i(1),
                    bit_o => shifted1(27 - i)                    
                );
        end generate;
        gen_shift2_0 : if (i >= 0) and (i < 4) generate
            shifter2_0_inst: mux_1b
                port map (
                    bitA_i => '0',
                    bitB_i => shifted1(27 - i),
                    sel_i => shift_i(2),
                    bit_o => shifted2(27 - i)                    
                );
        end generate;
        gen_shift2_i : if (i > 3) and (i < 28) generate
            shifter2_i_inst: mux_1b
                port map (
                    bitA_i => shifted1(27 - (i - 4)),
                    bitB_i => shifted1(27 - i),
                    sel_i => shift_i(2),
                    bit_o => shifted2(27 - i)                    
                );
        end generate;
        gen_shift3_0 : if (i >= 0) and (i < 8) generate
            shifter3_0_inst: mux_1b
                port map (
                    bitA_i => '0',
                    bitB_i => shifted2(27 - i),
                    sel_i => shift_i(3),
                    bit_o => shifted3(27 - i)                    
                );
        end generate;
        gen_shift3_i : if (i > 7) and (i < 28) generate
            shifter3_i_inst: mux_1b
                port map (
                    bitA_i => shifted2(27 - (i - 8)),
                    bitB_i => shifted2(27 - i),
                    sel_i => shift_i(3),
                    bit_o => shifted3(27- i)                    
                );
        end generate;
        gen_shift4_0 : if (i >= 0) and (i < 16) generate
            shifter4_0_inst: mux_1b
                port map (
                    bitA_i => '0',
                    bitB_i => shifted3(27 - i),
                    sel_i => shift_i(4),
                    bit_o => shifted4(27 - i)                    
                );
        end generate;
        gen_shift4_i : if (i > 15) and (i < 28) generate
            shifter4_i_inst: mux_1b
                port map (
                    bitA_i => shifted3(27 - (i - 16)),
                    bitB_i => shifted3(27 - i),
                    sel_i => shift_i(4),
                    bit_o => shifted4(27 - i)                    
                );
        end generate;
    end generate;
    shifted_o <= shifted4;
end architecture;