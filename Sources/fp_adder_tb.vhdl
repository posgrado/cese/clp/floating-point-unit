library ieee;
use ieee.std_logic_1164.all;

entity fp_adder_tb is
end entity;

architecture fp_adder_tb_arq of fp_adder_tb is
    component fp_adder is
        port (
            numberA_i   : in std_logic_vector(31 downto 0);
            numberB_i   : in std_logic_vector(31 downto 0);
            addSub_i : in std_logic;
            result_o    : out std_logic_vector(31 downto 0)
        );
    end component;
    
    signal numberA_tb   : std_logic_vector(31 downto 0);
    signal numberB_tb   : std_logic_vector(31 downto 0);
    signal addSub_tb    : std_logic;
    signal result_tb    : std_logic_vector(31 downto 0);
    
begin
    
    addSub_tb <= '0';
    numberA_tb <= "10000000000000000000000000000000",      -- -Zero
                "00000000000000000000000000000000" after 10 ns,      -- Zero
                "00000000000000000000000000000000" after 20 ns,      -- Zero
                "01000000000110011001100110011010" after 30 ns,      -- 2.4
                "01000000000000000000000000000000" after 40 ns,      -- 2
                "00000000000000000000000000000000" after 50 ns,      -- Zero
                "01000000000000000000001000000000" after 60 ns,      -- 2
                "00000000000110011001100110011010" after 70 ns,      -- 2.4
                "01111111100000000000000000000000" after 80 ns,      -- +Inf
                "11111111100000000000000000000000" after 90 ns,      -- -Inf
                "11111111100000000000000000000000" after 100 ns,      -- -Inf
                "11111111100000000000000000000000" after 110 ns,      -- -Inf
                "10000000000000000000000000000000" after 120 ns,      -- -Zero
                "11111111100000000000000000000001" after 130 ns,      -- NaN
                "01111111100000000000000000000010" after 140 ns,      -- NaN
                "11111111100000000000000000000011" after 150 ns,      -- NaN
                "01111111100000000000000000000100" after 160 ns,      -- NaN
                "11111111100000000000000000000101" after 170 ns,      -- NaN
                "01111111100000000000000000000110" after 180 ns,      -- NaN
                "01000000000000000000000000000000" after 190 ns,      -- 2
                "01111111100000000000000000000000" after 200 ns,      -- +Inf   
                "01000000000000000000000000000000" after 210 ns;      -- 2   
    numberB_tb <= "00000000000000000000000000000000",    -- Zero
                "10000000000000000000000000000000" after 10 ns,      -- -Zero
                "01000000000110011001100110011010" after 20 ns,      -- 2.4
                "00000000000000000000000000000000" after 30 ns,      -- Zero
                "00000000000000000000000000000000" after 40 ns,      -- Zero
                "01000000000000000000000000000000" after 50 ns,      -- 2
                "01000000000000000000000100000000" after 60 ns,      -- 2
                "00000000000110011001100110011010" after 70 ns,      -- 2.4
                "01111111100000000000000000000000" after 80 ns,      -- +Inf
                "01111111100000000000000000000000" after 90 ns,      -- +Inf
                "11111111100000000000000000000000" after 100 ns,      -- -Inf
                "00000000000000000000000000000000" after 110 ns,      -- +Zero
                "01111111100000000000000000000000" after 120 ns,      -- -Zero
                "11111111100000000000000000000001" after 130 ns,      -- NaN
                "01111111100000000000000000000000" after 140 ns,      -- -Zero
                "00000000000000000000000000000000" after 150 ns,      -- Zero
                "11111111100000000000000000000000" after 160 ns,      -- -Inf
                "01111111100000000000000000000000" after 170 ns,      -- +Inf
                "01000000000000000000000000000000" after 180 ns,      -- 2
                "11111111100000000000000000000111" after 190 ns,      -- NaN
                "11111111100000000000000000001000" after 200 ns,      -- NaN
                "01000000000000000000000000000000" after 210 ns;      -- 2
        
    DUT: fp_adder
    port map (
        numberA_i => numberA_tb,
        numberB_i => numberB_tb,
        addSub_i => addSub_tb,
        result_o => result_tb
    );
end architecture;