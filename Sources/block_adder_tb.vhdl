library ieee;
use ieee.std_logic_1164.all;

entity block_adder_tb is
end entity;

architecture block_adder_arq of block_adder_tb is
    component block_adder is
        port (
            sigA_i      : in std_logic;
            sigB_i      : in std_logic;
            numberA_i   : in std_logic_vector(27 downto 0);
            numberB_i   : in std_logic_vector(27 downto 0);
            addSub_i    : in std_logic;
            comp_i      : in std_logic;
            result_o    : out std_logic_vector(27 downto 0);
            sig_o       : out std_logic;
            carry_o       : out std_logic
        );
    end component;

    signal sigA_tb      :  std_logic;
    signal sigB_tb      :  std_logic;
    signal numberA_tb   :  std_logic_vector(27 downto 0);
    signal numberB_tb   :  std_logic_vector(27 downto 0);
    signal addSub_tb    :  std_logic;
    signal comp_tb      :  std_logic;
    signal result_tb    :  std_logic_vector(27 downto 0);
    signal sig_tb       :  std_logic;
    signal carry_tb     :  std_logic;

begin
    sigA_tb <=  '0',
                '0' after 10 ns,
                '0' after 20 ns,
                '0' after 30 ns,
                '1' after 40 ns,
                '1' after 50 ns,
                '1' after 60 ns,
                '1' after 70 ns;

    numberA_tb <=   "0000000000000000000000000010",
                    "0000000000000000000000000010" after 10 ns,
                    "0000000000000000000000000010" after 20 ns,
                    "0000000000000000000000000010" after 30 ns,
                    "0000000000000000000000000010" after 40 ns,
                    "0000000000000000000000000010" after 50 ns,
                    "0000000000000000000000000010" after 60 ns,
                    "0000000000000000000000000010" after 70 ns;
    sigB_tb <=  '0',
                '0' after 10 ns,
                '1' after 20 ns,
                '1' after 30 ns,
                '0' after 40 ns,
                '0' after 50 ns,
                '1' after 60 ns,
                '1' after 70 ns;

    numberB_tb <=   "0000000000000000000000000010",
                    "0000000000000000000000000010" after 10 ns,
                    "0000000000000000000000000010" after 20 ns,
                    "0000000000000000000000000010" after 30 ns,
                    "0000000000000000000000000010" after 40 ns,
                    "0000000000000000000000000010" after 50 ns,
                    "0000000000000000000000000010" after 60 ns,
                    "0000000000000000000000000010" after 70 ns;
    addSub_tb <= '0',
                 '1' after 10 ns,
                 '0' after 20 ns,
                 '1' after 30 ns,
                 '0' after 40 ns,
                 '1' after 50 ns,
                 '0' after 60 ns,
                 '1' after 70 ns;

    comp_tb <= '1';     -- 1 if A > B

    DUT: block_adder
    port map (
        sigA_i => sigA_tb,
        sigB_i => sigB_tb,
        numberA_i => numberA_tb,
        numberB_i => numberB_tb,
        addSub_i => addSub_tb,
        comp_i => comp_tb,
        result_o => result_tb,
        sig_o => sig_tb,
        carry_o => carry_tb
    );
end architecture;