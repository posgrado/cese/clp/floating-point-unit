library ieee;
use ieee.std_logic_1164.all;

entity top_adder is
    port (
        clk_i       : in std_logic := '0';
        numberA_i   : in std_logic_vector(31 downto 0);
        numberB_i   : in std_logic_vector(31 downto 0);
        addSub_i : in std_logic;
        result_o    : out std_logic_vector(31 downto 0)
    );
end entity;

architecture behavioral of top_adder is
    component fp_adder is
        port (
            numberA_i   : in std_logic_vector(31 downto 0);
            numberB_i   : in std_logic_vector(31 downto 0);
            addSub_i : in std_logic;
            result_o    : out std_logic_vector(31 downto 0)
        );
    end component;
    component vio is
        port (
          clk_0 : in STD_LOGIC;
          probe_in0_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
          probe_in1_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
          probe_in2_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
          probe_out0_0 : out STD_LOGIC_VECTOR ( 31 downto 0 )
        );
    end component;
begin
    vio_inst: vio
        port map (
            clk_0 => clk_i,
            probe_in0_0 => numberA_i,
            probe_in1_0 => numberB_i,
            probe_in2_0(0) => addSub_i,
            probe_out0_0 => result_o
        );

    fp_adder_inst: fp_adder
        port map (
            numberA_i => numberA_i,
            numberB_i => numberB_i,
            addSub_i => addSub_i,
            result_o => result_o 
        );
end architecture;