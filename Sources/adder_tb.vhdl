library ieee;
use ieee.std_logic_1164.all;

entity adder_tb is
end entity;

architecture adder_tb_arq of adder_tb is
    component adder is
        port (
            numberA_i   : in std_logic_vector(27 downto 0);        
            numberB_i   : in std_logic_vector(27 downto 0);        
            addSub_i    : in std_logic;
            result_o    : out std_logic_vector(27 downto 0);
            carry_o     : out std_logic 
        );
    end component;

    signal numberA_tb   : std_logic_vector(27 downto 0);        
    signal numberB_tb   : std_logic_vector(27 downto 0);        
    signal addSub_tb    : std_logic;
    signal result_tb    : std_logic_vector(27 downto 0);
    signal carry_tb     : std_logic; 

begin
    numberA_tb <= "0000000000000000000000000010";
    numberB_tb <= "0000000000000000000000000010";
    addSub_tb <= '0';

    DUT: adder
    port map (
        numberA_i => numberA_tb,
        numberB_i => numberB_tb,
        addSub_i => addSub_tb,
        result_o => result_tb,
        carry_o => carry_tb
    );
end architecture;