library ieee;
use ieee.std_logic_1164.all;

entity norm_tb is
end entity;

architecture norm_tb_arq of norm_tb is
    component norm is
        port (
            numberA_i   : in std_logic_vector(36 downto 0);        
            numberB_i   : in std_logic_vector(36 downto 0);        
            normA_o   : out std_logic_vector(36 downto 0);        
            normB_o   : out std_logic_vector(36 downto 0)
        );
    end component;

    signal numberA_tb   : std_logic_vector(36 downto 0);        
    signal numberB_tb   : std_logic_vector(36 downto 0);        
    signal normA_tb     : std_logic_vector(36 downto 0);        
    signal normB_tb     : std_logic_vector(36 downto 0);

begin

    numberA_tb <=   "0100000001000000000000000000000000000",
                    "0000000000010000000000000000000000000" after 10 ns;
    numberB_tb <=   "0000000000010000000000000000000000000",
                    "0100000001000000000000000000000000000" after 10 ns;

    DUT: norm
    port map (
        numberA_i => numberA_tb,
        numberB_i => numberB_tb,
        normA_o => normA_tb,
        normB_o => normB_tb
    );
end architecture;