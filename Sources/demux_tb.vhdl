library ieee;
use ieee.std_logic_1164.all;

entity demux_tb is
end entity;

architecture demux_tb_arq of demux_tb is
    component demux is
        port (
            numberA_i       : in std_logic_vector(36 downto 0);        
            numberB_i       : in std_logic_vector(36 downto 0); 
            enable_type_i   : in std_logic_vector(1 downto 0);       
            numberA0_o       : out std_logic_vector(36 downto 0);        
            numberB0_o       : out std_logic_vector(36 downto 0); 
            numberA1_o       : out std_logic_vector(36 downto 0);        
            numberB1_o       : out std_logic_vector(36 downto 0); 
            numberA2_o       : out std_logic_vector(36 downto 0);        
            numberB2_o       : out std_logic_vector(36 downto 0)
        );
    end component;


    signal numberA_tb       : std_logic_vector(36 downto 0);        
    signal numberB_tb       : std_logic_vector(36 downto 0); 
    signal enable_type_tb   : std_logic_vector(1 downto 0);       
    signal numberA0_tb       :  std_logic_vector(36 downto 0);        
    signal numberB0_tb       :  std_logic_vector(36 downto 0); 
    signal numberA1_tb       :  std_logic_vector(36 downto 0);        
    signal numberB1_tb       :  std_logic_vector(36 downto 0); 
    signal numberA2_tb       :  std_logic_vector(36 downto 0);        
    signal numberB2_tb       :  std_logic_vector(36 downto 0);

begin


    numberA_tb <=   "0100000001001100110011001100110100000",                 -- 2.4 (normal)
                    "0100000001000000000000000000000000000" after 10 ns,     -- 2 (normal)
                    "0000000000100000000000000000000000000" after 20 ns,     -- 2.93873587706e-39 (subnormal)
                    "0100000001000000000000000000000000000" after 30 ns;     -- 2 (normal)
                    
    numberB_tb <=   "0100000001001100110011001100110100000",                 -- 2.4 (normal
                    "0100000001000000000000000000000000000" after 10 ns,     -- 2 (normal)
                    "0000000000010000000000000000000000000" after 20 ns,     -- 2.93873587706e-39 (subnormal)
                    "0000000000010000000000000000000000000" after 30 ns;     -- 2.93873587706e-39 (subnormal)
    
    enable_type_tb <=   "01",                 -- Normals
                        "01" after 10 ns,     -- Normals
                        "00" after 20 ns,     -- Subnormals
                        "10" after 30 ns;     -- Mixed
    DUT: demux
    port map (
        numberA_i => numberA_tb,
        numberB_i => numberB_tb,
        enable_type_i => enable_type_tb,
        numberA0_o => numberA0_tb,
        numberB0_o => numberB0_tb,
        numberA1_o => numberA1_tb,
        numberB1_o => numberB1_tb,
        numberA2_o => numberA2_tb,
        numberB2_o => numberB2_tb
    );

end architecture;