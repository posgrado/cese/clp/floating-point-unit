library ieee;
use ieee.std_logic_1164.all;

entity round_tb is
end entity;

architecture round_tb_arq of round_tb is
    component round is
        port (
            man_i   : in std_logic_vector(27 downto 0);
            man_o   : out   std_logic_vector(22 downto 0)   
        );
    end component;
    signal man_i_tb   : std_logic_vector(27 downto 0);
    signal man_o_tb   : std_logic_vector(22 downto 0);
begin

    man_i_tb <= "0000000000000000000000000000",
                "0000000000000000000000000111" after 50 ns,
                "0000000000000000000000001000" after 100 ns,
                "0000000000000000000000001001" after 150 ns;

    DUT: round
    port map (
        man_i => man_i_tb,
        man_o => man_o_tb
    );
end architecture;