library ieee;
use ieee.std_logic_1164.all;

entity singout_tb is
end entity;

architecture singout_tb_arq of singout_tb is
    component signout    is
        port (
            sigA_i      : in std_logic;
            sigB_i      : in std_logic;
            numberA_i   : in std_logic_vector(27 downto 0);
            numberB_i   : in std_logic_vector(27 downto 0);
            addSub_i    : in std_logic;
            comp_i      : in std_logic;
            numberA_o   : out std_logic_vector(27 downto 0);
            numberB_o   : out std_logic_vector(27 downto 0);
            addSub_o    : out std_logic;
            sig_o      : out std_logic
        );
    end component;

    signal sigA_tb      :  std_logic;
    signal sigB_tb      :  std_logic;
    signal numberA_i_tb   :  std_logic_vector(27 downto 0);
    signal numberB_i_tb   :  std_logic_vector(27 downto 0);
    signal addSub_i_tb    :  std_logic;
    signal comp_tb      :  std_logic;
    signal numberA_o_tb   :  std_logic_vector(27 downto 0);
    signal numberB_o_tb   :  std_logic_vector(27 downto 0);
    signal addSub_o_tb    :  std_logic;
    signal sig_tb    :  std_logic;

begin
    sigA_tb <= '0';
    numberA_i_tb <= "0000000000000000000000000010";
    sigB_tb <= '0';
    numberB_i_tb <= "0000000000000000000000000010";
    addSub_i_tb <= '0', '1' after 100 ns;
    comp_tb <= '0';

    DUT: signout
    port map (
        sigA_i => sigA_tb,
        sigB_i => sigB_tb,
        numberA_i => numberA_i_tb,
        numberB_i => numberB_i_tb,
        numberA_o => numberA_o_tb,
        numberB_o => numberB_o_tb,
        addSub_i => addSub_i_tb,
        addSub_o => addSub_o_tb,
        comp_i => comp_tb,
        sig_o => sig_tb
    );
end architecture;