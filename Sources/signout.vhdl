library ieee;
use ieee.std_logic_1164.all;

entity signout is
    port (
        sigA_i      : in std_logic;
        sigB_i      : in std_logic;
        numberA_i   : in std_logic_vector(27 downto 0);
        numberB_i   : in std_logic_vector(27 downto 0);
        addSub_i    : in std_logic;
        comp_i      : in std_logic;
        numberA_o   : out std_logic_vector(27 downto 0);
        numberB_o   : out std_logic_vector(27 downto 0);
        addSub_o    : out std_logic;
        sig_o      : out std_logic
    );
end entity;

architecture behavioral of signout is
    signal sigB_aux : std_logic;
    signal numberA_aux : std_logic_vector(27 downto 0);
    signal numberB_aux : std_logic_vector(27 downto 0);
begin
    sigB_aux <= sigB_i xor addSub_i;    -- Set sign according to operation

    sig_o <= sigA_i when comp_i = '1' else  -- A > B --> Sign A
            sigB_aux when comp_i = '0' else -- B > A --> Sing B xor addSub
            '-';
            
    addSub_o <= '1' when sigA_i /= sigB_aux else -- Change operation when signs are differents
                '0';         

    numberA_aux <= numberA_i when comp_i = '1' else
                numberB_i when comp_i  = '0' else
                "----------------------------";

    numberB_aux <= numberB_i when comp_i = '1' else
                numberA_i when comp_i  = '0' else
                "----------------------------";
            
    process (sigA_i, sigB_aux, numberA_aux, numberB_aux)
    begin
        if (sigA_i xor sigB_aux) = '0' then
            numberA_o <= numberA_aux;
            numberB_o <= numberB_aux;
        elsif sigA_i = '1' and sigB_aux = '0' then
            numberA_o <= numberB_aux;   -- In this case, swap numbers
            numberB_o <= numberA_aux;
        elsif sigA_i = '0' and sigB_aux = '1' then
            numberA_o <= numberA_aux;
            numberB_o <= numberB_aux;
        else
            numberA_o <= "----------------------------";      
            numberB_o <= "----------------------------";      
        end if;
    end process;
end architecture;
