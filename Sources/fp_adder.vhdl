library ieee;
use ieee.std_logic_1164.all;

entity fp_adder is
    port (
        numberA_i   : in std_logic_vector(31 downto 0);
        numberB_i   : in std_logic_vector(31 downto 0);
        addSub_i : in std_logic;
        result_o    : out std_logic_vector(31 downto 0)
    );
end entity;

architecture behavioral of fp_adder is

    component pre_adder is
        port (
            numberA_i   : in std_logic_vector(31 downto 0);
            numberB_i   : in std_logic_vector(31 downto 0);
            enable_i    : in std_logic;
            sigA_o      : out std_logic;
            sigB_o      : out std_logic;
            comp_o      : out std_logic;
            exp_o       : out std_logic_vector(7 downto 0);
            manA_o       : out std_logic_vector(27 downto 0);
            manB_o       : out std_logic_vector(27 downto 0)
        );
    end component;

    component block_adder is
        port (
            sigA_i      : in std_logic;
            sigB_i      : in std_logic;
            numberA_i   : in std_logic_vector(27 downto 0);
            numberB_i   : in std_logic_vector(27 downto 0);
            addSub_i    : in std_logic;
            comp_i      : in std_logic;
            result_o    : out std_logic_vector(27 downto 0);
            sig_o       : out std_logic;
            carry_o       : out std_logic   
        );
    end component;

    component block_norm is
        port (
            man_i   : in std_logic_vector(27 downto 0);
            exp_i   : in std_logic_vector(7 downto 0);
            carry_i : in std_logic;
            man_o   : out std_logic_vector(22 downto 0);
            exp_o   : out std_logic_vector(7 downto 0)
        );
    end component;

    component vector is
        port (
            sig_i   : in std_logic;
            exp_i   : in std_logic_vector(7 downto 0);
            man_i   : in std_logic_vector(22 downto 0);
            vector_o    : out std_logic_vector(31 downto 0)  
        );
    end component;

    component n_case is
        port (
            numberA_i   : in std_logic_vector(31 downto 0);
            numberB_i   : in std_logic_vector(31 downto 0);
            sum_o   : out std_logic_vector(31 downto 0);
            enable_o : out std_logic
        );
    end component;

    component mux_fpadder is
        port (
            n_case_number_i : in std_logic_vector(31 downto 0);
            adder_number_i  : in std_logic_vector(31 downto 0);
            enable_i        : in std_logic;
            result_o        : out std_logic_vector(31 downto 0)
        );
    end component;

    signal enable_aux : std_logic;
    signal manA_aux : std_logic_vector(27 downto 0);
    signal manB_aux : std_logic_vector(27 downto 0);
    signal exp_aux : std_logic_vector(7 downto 0);
    signal comp_aux : std_logic;
    signal sigA_aux : std_logic;
    signal sigB_aux : std_logic;
    signal n_case_number : std_logic_vector(31 downto 0);
    signal adder_number : std_logic_vector(31 downto 0);
    signal result_adder : std_logic_vector(27 downto 0);
    signal sig_adder    : std_logic;
    
    signal man_norm : std_logic_vector(22 downto 0);
    signal exp_norm : std_logic_vector(7 downto 0);
    
    signal carry_adder  : std_logic;

begin

    pre_adder_inst: pre_adder
        port map (
            numberA_i => numberA_i,
            numberB_i => numberB_i,
            enable_i => enable_aux,
            sigA_o => sigA_aux,
            sigB_o => sigB_aux,
            comp_o => comp_aux,
            exp_o => exp_aux,
            manA_o => manA_aux,
            manB_o => manB_aux
        );

    block_adder_inst: block_adder
        port map (
            sigA_i => sigA_aux,
            sigB_i => sigB_aux,
            numberA_i => manA_aux,
            numberB_i => manB_aux,
            addSub_i => addSub_i,
            comp_i => comp_aux,
            result_o => result_adder,
            sig_o => sig_adder,
            carry_o => carry_adder
        );

    block_norm_inst: block_norm
        port map (
            man_i => result_adder,
            exp_i => exp_aux,
            carry_i => carry_adder,
            man_o => man_norm,
            exp_o => exp_norm
        );
    vector_inst: vector
        port map (
            sig_i => sig_adder,
            exp_i  => exp_norm,
            man_i  => man_norm,
            vector_o => adder_number
        );

    mux_fpadder_inst: mux_fpadder
        port map (
            n_case_number_i => n_case_number,
            adder_number_i => adder_number,
            enable_i => enable_aux,
            result_o => result_o
        );

    n_case_inst: n_case
        port map (
            numberA_i => numberA_i,
            numberB_i => numberB_i,
            enable_o => enable_aux,
            sum_o => n_case_number
        );
end architecture;