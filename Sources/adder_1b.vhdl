library ieee;
use ieee.std_logic_1164.all;

entity adder_1b is
    port (
        bitA_i  : in std_logic;
        bitB_i  : in std_logic;
        carry_i  : in std_logic;
        sum_o  : out std_logic;
        carry_o  : out std_logic
    );
end entity;

architecture behavioral of adder_1b is
    
    signal carry_gen : std_logic;
    signal carry_prop : std_logic;
    
begin

    carry_gen <= bitA_i and bitB_i;     -- Carry generation
    carry_prop <= bitA_i xor bitB_i;     -- Carry propagation

    carry_o <= carry_gen or (carry_prop and carry_i);
    sum_o <= carry_prop xor carry_i;

end architecture;