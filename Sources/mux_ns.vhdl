library ieee;
use ieee.std_logic_1164.all;

entity mux_ns is
    port (
        normalA_i       : in std_logic_vector(36 downto 0);        
        normalB_i       : in std_logic_vector(36 downto 0);        
        mixedA_i        : in std_logic_vector(36 downto 0);        
        mixedB_i        : in std_logic_vector(36 downto 0);  
        enable_type_i   : in std_logic_vector(1 downto 0);
        numberA_o       : out std_logic_vector(36 downto 0);  
        numberB_o       : out std_logic_vector(36 downto 0)
    );
end entity;

architecture behavioral of mux_ns is
    
begin

    numberA_o <=    normalA_i when enable_type_i = "01" else
                    mixedA_i  when enable_type_i = "10" else
                    "-------------------------------------"; 

    numberB_o <=    normalB_i when enable_type_i = "01" else
                    mixedB_i  when enable_type_i = "10" else
                    "-------------------------------------"; 

end architecture;