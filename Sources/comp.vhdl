library ieee;
use ieee.std_logic_1164.all;

entity comp is
    port (
        numberA_i   : in std_logic_vector(36 downto 0);
        numberB_i   : in std_logic_vector(36 downto 0);
        normal_o   : out std_logic_vector(36 downto 0);
        subnormal_o   : out std_logic_vector(36 downto 0)
    );
end entity;

architecture behavioral of comp is
    
    signal expA : std_logic_vector(7 downto 0);
    signal expB : std_logic_vector(7 downto 0);
    
begin
    expA <= numberA_i(35 downto 28);
    expB <= numberB_i(35 downto 28);

    process (numberA_i, numberB_i, expA, expB)
    begin
        if expA = X"00" then
            subnormal_o <= numberA_i;
            normal_o <= numberB_i;
        elsif expB = X"00" then
            subnormal_o <= numberB_i;
            normal_o <= numberA_i;
        else
            subnormal_o <= "-------------------------------------";    
            normal_o <= "-------------------------------------";    
        end if;        
    end process;
end architecture;