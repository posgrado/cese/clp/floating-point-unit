library ieee;
use ieee.std_logic_1164.all;

entity comp_exp_tb is
end entity;

architecture comp_exp_tb of comp_exp_tb is
    component comp_exp is
        port (
            numberA_i   : in std_logic_vector(36 downto 0);        
            numberB_i   : in std_logic_vector(36 downto 0);        
            sigA_o      : out std_logic;
            sigB_o      : out std_logic;
            max_exp_o   : out std_logic_vector(7 downto 0);
            max_man_o   : out std_logic_vector(27 downto 0);
            shift_man_o : out std_logic_vector(27 downto 0);
            exp_diff_o  : out std_logic_vector(4 downto 0);
            comp_o      : out std_logic
        );
    end component;
    signal numberA_tb   : std_logic_vector(36 downto 0);        
    signal numberB_tb   : std_logic_vector(36 downto 0);        
    signal sigA_tb      : std_logic;
    signal sigB_tb      : std_logic;
    signal max_exp_tb   : std_logic_vector(7 downto 0);
    signal max_man_tb   : std_logic_vector(27 downto 0);
    signal shift_man_tb : std_logic_vector(27 downto 0);
    signal exp_diff_tb  : std_logic_vector(4 downto 0);
    signal comp_tb      : std_logic;
begin

    numberA_tb <=   "0000010000000000000001000000000000000",
                    "0000001000000000000000010000000000000" after 10 ns;
    numberB_tb <=   "0000001000000000000000010000000000000",
                    "0000010000000000000001000000000000000" after 10 ns;

    DUT: comp_exp
    port map (
        numberA_i => numberA_tb,
        numberB_i => numberB_tb,
        sigA_o => sigA_tb,
        sigB_o => sigB_tb,
        max_exp_o => max_exp_tb,
        max_man_o => max_man_tb,
        shift_man_o => shift_man_tb,
        exp_diff_o => exp_diff_tb,
        comp_o => comp_tb
    );

end architecture;