library ieee;
use ieee.std_logic_1164.all;

entity mux_fpadder is
    port (
        n_case_number_i : in std_logic_vector(31 downto 0);
        adder_number_i  : in std_logic_vector(31 downto 0);
        enable_i        : in std_logic;
        result_o        : out std_logic_vector(31 downto 0)
    );
end entity;

architecture behavioral of mux_fpadder is
    
begin
    result_o <= n_case_number_i when enable_i = '0' else
                adder_number_i when enable_i = '1' else
                "--------------------------------";
end architecture;