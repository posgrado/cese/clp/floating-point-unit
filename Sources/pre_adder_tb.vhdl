library ieee;
use ieee.std_logic_1164.all;

entity pre_adder_tb is
end entity;

architecture pre_adder_tb_arq of pre_adder_tb is

    component pre_adder is
        port (
            numberA_i   : in std_logic_vector(31 downto 0);
            numberB_i   : in std_logic_vector(31 downto 0);
            enable_i    : in std_logic;
            sigA_o      : out std_logic;
            sigB_o      : out std_logic;
            comp_o      : out std_logic;
            exp_o       : out std_logic_vector(7 downto 0);
            manA_o       : out std_logic_vector(27 downto 0);
            manB_o       : out std_logic_vector(27 downto 0)
        );
    end component;
    
    signal numberA_tb    : std_logic_vector(31 downto 0);
    signal numberB_tb    : std_logic_vector(31 downto 0);
    signal enable_tb     : std_logic;
    signal sigA_tb       : std_logic;
    signal sigB_tb       : std_logic;
    signal comp_tb       : std_logic;
    signal exp_tb        : std_logic_vector(7 downto 0);
    signal manA_tb       : std_logic_vector(27 downto 0);
    signal manB_tb       : std_logic_vector(27 downto 0);

begin

    numberA_tb <=   "01000000000110011001100110011010",
                    "01000000000000000000000000000000" after 20 ns,
                    "01000000000000000000001000000000" after 30 ns;
    numberB_tb <=   "01000000000110011001100110011010",
                    "01000000000000000000000000000000" after 20 ns,
                    "01000000000000000000001000000000" after 30 ns;
    enable_tb <= '1';
    
    DUT: pre_adder
    port map (
        numberA_i => numberA_tb,
        numberB_i => numberB_tb,
        enable_i => enable_tb,
        sigA_o => sigA_tb,
        sigB_o => sigB_tb,
        comp_o => comp_tb,
        exp_o => exp_tb,
        manA_o => manA_tb,
        manB_o => manB_tb
    );
    

end architecture;