library ieee;
use ieee.std_logic_1164.all;

entity block_norm_tb is
end entity;

architecture block_norm_tb_arq of block_norm_tb is
    component block_norm is
        port (
            man_i   : in std_logic_vector(27 downto 0);
            exp_i   : in std_logic_vector(7 downto 0);
            carry_i : in std_logic;
            man_o   : out std_logic_vector(22 downto 0);
            exp_o   : out std_logic_vector(7 downto 0)
        );
    end component;
    
    signal man_i_tb   : std_logic_vector(27 downto 0);
    signal exp_i_tb   : std_logic_vector(7 downto 0);
    signal carry_i_tb : std_logic;
    signal man_o_tb   : std_logic_vector(22 downto 0);
    signal exp_o_tb   : std_logic_vector(7 downto 0);
begin

    man_i_tb <= "0000000000000000000000000110";
    exp_i_tb <= "00100000";
    carry_i_tb <= '0';

    DUT: block_norm
    port map (
        man_i => man_i_tb,
        exp_i => exp_i_tb,
        carry_i => carry_i_tb,
        man_o => man_o_tb,
        exp_o => exp_o_tb
    );
end architecture;