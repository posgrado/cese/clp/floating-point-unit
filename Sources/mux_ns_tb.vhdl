library ieee;
use ieee.std_logic_1164.all;

entity mux_ns_tb is
end entity;

architecture mux_ns_tb_arq of mux_ns_tb is
    component mux_ns is
        port (
            normalA_i       : in std_logic_vector(36 downto 0);        
            normalB_i       : in std_logic_vector(36 downto 0);        
            mixedA_i        : in std_logic_vector(36 downto 0);        
            mixedB_i        : in std_logic_vector(36 downto 0);  
            enable_type_i   : in std_logic_vector(1 downto 0);
            numberA_o       : out std_logic_vector(36 downto 0);  
            numberB_o       : out std_logic_vector(36 downto 0)
        );
    end component;
    
    signal normalA_tb       : std_logic_vector(36 downto 0);        
    signal normalB_tb       : std_logic_vector(36 downto 0);        
    signal mixedA_tb        : std_logic_vector(36 downto 0);        
    signal mixedB_tb        : std_logic_vector(36 downto 0);  
    signal enable_type_tb   : std_logic_vector(1 downto 0);
    signal numberA_tb       : std_logic_vector(36 downto 0);  
    signal numberB_tb       : std_logic_vector(36 downto 0);
    
begin

    normalA_tb <= "0100000001001100110011001100110100000";
    normalB_tb <= "0100000001001100110011001100110100000";
    enable_type_tb <= "01";

    DUT: mux_ns
    port map (
        normalA_i => normalA_tb,
        normalB_i => normalB_tb,
        mixedA_i => mixedA_tb,
        mixedB_i => mixedB_tb,
        enable_type_i => enable_type_tb,
        numberA_o => numberA_tb,
        numberB_o => numberB_tb
    );

end architecture;