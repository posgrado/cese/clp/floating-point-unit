LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;

entity n_case is
    port (
        numberA_i   : in std_logic_vector(31 downto 0);
        numberB_i   : in std_logic_vector(31 downto 0);
        sum_o   : out std_logic_vector(31 downto 0);
        enable_o : out std_logic
    );
end entity;

architecture behavioral of n_case is
    signal outA : std_logic_vector(2 downto 0);
    signal outB : std_logic_vector(2 downto 0);
    signal expA : std_logic_vector(7 downto 0);
    signal expB : std_logic_vector(7 downto 0);
    signal manA : std_logic_vector(22 downto 0);
    signal manB : std_logic_vector(22 downto 0);
    signal sigA : std_logic;
    signal sigB : std_logic;

    signal expSum : std_logic_vector(7 downto 0);
    signal manSum : std_logic_vector(22 downto 0);
    signal sigSum : std_logic;
    
begin
    sigA <= numberA_i(31);
    sigB <= numberB_i(31);
    expA <= numberA_i(30 downto 23);
    expB <= numberB_i(30 downto 23);
    manA <= numberA_i(22 downto 0);
    manB <= numberB_i(22 downto 0);

    outA <= "000" when expA = X"00" and manA = 0 else                       -- Zero
            "001" when expA = X"00" and manA > 0 else                       -- Subnormal
            "011" when (expA > X"00" and expA < X"FF") and manA >= 0 else    -- Normal
            "100" when expA = X"FF" and manA = 0 else                       -- Infinity
            "100" when expA = X"FF" and manA > 0 else                       -- NaN
            "000";
    
    outB <= "000" when expB = X"00" and manB = 0 else                       -- Zero
            "001" when expB = X"00" and manB > 0 else                       -- Subnormal
            "011" when (expB > X"00" and expB < X"FF") and manB >= 0 else    -- Normal
            "100" when expB = X"FF" and manB = 0 else                       -- Infinity
            "100" when expB = X"FF" and manB > 0 else                       -- NaN
            "000";

    -- If A and B are normal or subnormal numbers, enable_o = 1
    -- If not, enable_o = 0

    enable_o <= '1' when ((outA(0) and outB(0)) = '1') else '0';

    process (sigA, sigB, outA, outB)
    begin
        -- Zero
        if (outA = "000") then                  -- Zero +/- numberB_i
            sigSum <= sigB;
            expSum <= expB;
            manSum <= manB;
        elsif (outB = "000") then               -- numberA_i +/- Zero
            sigSum <= sigA;
            expSum <= expA;
            manSum <= manA;
        end if;
        -- Infinite
        if (outA(0) = '1' and outB = "100") then     -- Normal or Subnormal +/- Infinity
            sigSum <= sigB;
            expSum <= expB;
            manSum <= manB;
        elsif (outB(0) = '1' and outA = "100") then  -- Infinity +/- Normal or Subnormal
            sigSum <= sigA;
            expSum <= expA;
            manSum <= manA;
        end if;
        
        if ((outA and outB) = "100" and sigA = sigB) then     -- +/- Infinity +/- Infinity
            sigSum <= sigA;
            expSum <= expA;
            manSum <= manA;
        -- NaN
        elsif ((outA and outB) = "100" and sigA /= sigB) then     -- + Infinity - Infinity
            sigSum <= '1';
            expSum <= X"FF";
            manSum <= "00000000000000000000001";
        end if;
        if (outA = "110" or outB = "110") then     
            sigSum <= '1';
            expSum <= X"FF";
            manSum <= "00000000000000000000001";
        end if;
        -- Normal / Subnormal
        if ((outA(0) and outB(0)) = '1') then     
            sigSum <= '-';
            expSum <= "--------";
            manSum <= "-----------------------";
        end if;        
    end process;
    sum_o(31) <= sigSum;
    sum_o(30 downto 23) <= expSum;
    sum_o(22 downto 0) <= manSum;
end behavioral;