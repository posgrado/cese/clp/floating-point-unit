## Representación de números
### Números normales

- `Signo = 0` para número positivo
- `Signo = 1` para número negativo
- `Exponente > 0`
- `Mantisa mayor o igual a 0`

### Números subnormales

- `Signo = 0` para número positivo
- `Signo = 1` para número negativo
- `Exponente > 0`
- `Mantisa mayor o igual a 0`

### Cero (positivo y negativo)

- `Signo = 0` para cero positivo
- `Signo = 1` para cero negativo
- `Exponente = todos 0`
- `Mantisa = todos 0`

## Representación de no números
### Infinito (positivo y negativo)

- `Signo = 0` para infinito positivo
- `Signo = 1` para infinito negativo
- `Exponente = todos 1`
- `Mantisa = 0`

### NaN

- `Signo = 0 ó 1`
- `Exponente = todos 1`
- `Mantisa mayor a 0`

## Bloque `n_case.vhdl`
Es el bloque de entrada y lo que hace es analizar los números que se ingresan para saber si
realmente es necesario pasarlos por el sumador.

### Simulación del bloque `n_case.vhdl`
![](docs/img/special_num_sim.png)